<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home.home');
});

Route::get('home', function () {
    return view('home.home');
});

Route::get('about', function () {
    return view('about.about');
});

Route::get('room', function () {
    return view('room.room');
});

Route::get('room-dynamic', function () {
    return view('room.room-dynamic');
});

Route::get('package', function () {
    return view('package.package');
});

Route::get('package-dynamic', function () {
    return view('package.package-dynamic');
});

Route::get('activity', function () {
    return view('activity.activity');
});

Route::get('activity-dynamic', function () {
    return view('activity.activity-dynamic');
});

Route::get('tariff', function () {
    return view('tariff.tariff');
});

Route::get('album', function () {
    return view('album.album');
});

Route::get('gallery', function () {
    return view('album.gallery');
});

Route::get('contact', function () {
    return view('contact.contact');
});



