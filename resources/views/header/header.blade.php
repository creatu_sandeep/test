<nav class="navbar navbar-expand-lg meghauli-nav">
	<a class="navbar-brand meghauli-brand" href="#"><img src="{{url('public/images/meghauli.png')}}" alt="" class="img-fluid"></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse meghauli-menu" id="navbarNav">
		<ul class="navbar-nav">
			<li class="nav-item active">
				<a class="nav-link" href="{{url('/')}}">Home <span class="sr-only">(current)</span></a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{url('about')}}">About Us</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{url('room')}}">Rooms</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{url('package')}}">Packages</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{url('activity')}}">Activities</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{url('tariff')}}">Tariff</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{url('album')}}">Album</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{url('contact')}}">Contact Us</a>
			</li>
			<li class="nav-item">
				<a class="nav-link meghauli-menu-btn" data-toggle="modal" data-target="#squarespaceModal" href="#">Book Room</a>
			</li>
			<li class="nav-item">
				<a class="nav-link meghauli-menu-btn" href="#">Book Packages</a>
			</li>
		</ul>
	</div>
</nav>






<div class="modal fade" id="squarespaceModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="lineModalLabel">My Modal</h3>
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			</div>
			<div class="modal-body">

				<!-- content goes here -->
				<div class="container">
					<div class="row">

						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb30">
							<div class="tour-booking-form">
								<form>
									<div class="row">
										<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
											<h4 class="tour-form-title">Your Travel Plan Detail</h4>
										</div>
										<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
											<div class="form-group">
												<label class="control-label required" for="select">Destination</label>
												<div class="select">
													<select id="select" name="select" class="form-control">
														<option value="">Where you want to go</option>
														<option value="">Singapore</option>
														<option value="">Thailand</option>
														<option value="">Vietnam</option>
													</select>
												</div>
											</div>
										</div>
										<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
											<div class="form-group">
												<label class="control-label" for="datepicker">Check in</label>
												<div class="input-group">
													<input id="datepicker" name="datepicker" type="text" placeholder="Date" class="form-control" required>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span> </div>
												</div>
											</div>
											<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
												<div class="form-group">
													<label class="control-label required" for="select">Number of Persons:</label>
													<div class="select">
														<select id="select" name="select" class="form-control">
															<option value="">Number of Persons:</option>
															<option value="">01</option>
															<option value="">02</option>
															<option value="">03</option>
														</select>
													</div>
												</div>
											</div>
											<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
												<div class="form-group">
													<label class="control-label required" for="select">Budgets</label>
													<div class="select">
														<select id="select" name="select" class="form-control">
															<option value="">Stadard</option>
															<option value="">Stadard</option>
															<option value="">Stadard</option>
														</select>
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt30">
												<h4 class="tour-form-title">Your Travel Plan Detail</h4>
											</div>
											<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
												<div class="form-group">
													<label class="control-label" for="name">Name</label>
													<input id="name" type="text" placeholder="First Name" class="form-control" required>
												</div>
											</div>
											<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
												<div class="form-group">
													<label class="control-label" for="email"> Email</label>
													<input id="email" type="text" placeholder="xxxx@xxxx.xxx" class="form-control" required>
												</div>
											</div>
											<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
												<div class="form-group">
													<label class="control-label" for="phone"> Phone</label>
													<input id="phone" type="text" placeholder="(222) 222-2222" class="form-control" required>
												</div>
											</div>
											<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
												<div class="form-group">
													<label class="control-label" for="country">Country</label>
													<input id="country" type="text" placeholder="India" class="form-control" required>
												</div>
											</div>
											<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
												<div class="form-group">
													<label class="control-label" for="city">City</label>
													<input id="city" type="text" placeholder="Ahmedabad" class="form-control" required>
												</div>
											</div>
											<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
												<div class="form-group">
													<label class="control-label" for="textarea">Describe Your Travel Requirements</label>
													<textarea class="form-control" id="textarea" name="textarea" rows="4" placeholder="Write Your Requirements"></textarea>
												</div>
											</div>
											<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
												<button type="submit" name="singlebutton" class="btn btn-primary">send Enquiry</button>
											</div>
										</div>
									</form>
								</div>

							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>





	<!-- fb messenger -->
	<div class="fb-livechat">
		<div class="ctrlq fb-overlay"></div>
		<div class="fb-widget">
			<div class="ctrlq fb-close"></div>
			<div class="fb-page" data-href="https://www.facebook.com/pacificdatalimited/" data-tabs="messages" data-width="360" data-height="400" data-small-header="true" data-hide-cover="true" data-show-facepile="false">
				<blockquote cite="https://www.facebook.com/pacificdatalimited/" class="fb-xfbml-parse-ignore"> </blockquote>
			</div>
			<div class="fb-credit"> 
				<a href="https://www.facebook.com/pacificdatalimited/" target="_blank">Facebook Chat</a>
			</div>
			<div id="fb-root"></div>
		</div>
		<a href="https://www.facebook.com/pacificdatalimited/" title="Send us a message on Facebook" class="ctrlq fb-button"></a> 
	</div>  