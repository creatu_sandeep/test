@extends('home-master')

<!-- page title -->
@section('page-title')	

@endsection


<!-- website content -->
@section('content')
<div class="container margin-tb">
	<div class="row">
		<div class="col-md-6">
			<div class="item home-package-card">
				<p>1 Night / 2 Days</p>
				<h1>Package</h1>
			</div>
		</div>
		<div class="col-md-6">
			<div class="item home-package-card">
				<p>1 Night / 2 Days</p>
				<h1>Package</h1>
			</div>
		</div>
		<div class="col-md-6">
			<div class="item home-package-card">
				<p>1 Night / 2 Days</p>
				<h1>Package</h1>
			</div>
		</div>
		<div class="col-md-6">
			<div class="item home-package-card">
				<p>1 Night / 2 Days</p>
				<h1>Package</h1>
			</div>
		</div>
	</div>
</div>
@endsection