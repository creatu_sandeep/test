@extends('home-master')

@section('page-title')	
Home | Server Change
@endsection

@section('content')
<div class="container margin-tb">
	<div class="row">
		<div class="col-md-4">
			<div class="home-intro-logo">
				<img src="{{url('public/images/meghauli.png')}}" alt="" class="img-fluid">
			</div>
		</div>
		<div class="col-md-8">
			<div class="home-intro-text">
				<h1>Learn about meghauli resort</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure olor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
			<a href="{{url('#')}}" class="meghauli-btn-1"><p>Learn More</p></a>
		</div>
	</div>
</div>
<!-- home intro ends -->





<div class="container-fluid padding-0">
	<div id="owl-home-package">
		<div class="item home-package-card">
			<p>1 Night / 2 Days</p>
			<h1>Package</h1>
		</div>
		<div class="item home-package-card">
			<p>1 Night / 2 Days</p>
			<h1>Package</h1>
		</div>
		<div class="item home-package-card">
			<p>1 Night / 2 Days</p>
			<h1>Package</h1>
		</div>
		<div class="item home-package-card">
			<p>1 Night / 2 Days</p>
			<h1>Package</h1>
		</div>
	</div>
</div>
<!-- home package ends -->




<div class="container-fluid">
	<div class="row">
		<div class="col-md-6 padding-0">
			<div class="home-room-image">
				<img src="{{url('public/images/2.jpg')}}" alt="" class="img-fluid">
			</div>
		</div>
		<div class="col-md-6 home-room-card">
			<div class="home-room-text">
				<h1>Suite</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure olor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
			<ul>
				<li><a href="{{url('#')}}" class="meghauli-btn-1"><p>Learn More</p></a></li>
				<li><a href="{{url('#')}}" class="meghauli-btn-1"><p>Learn More</p></a></li>
			</ul>
		</div>
	</div>
</div>


<div class="container-fluid">
	<div class="row">
		<div class="col-md-6 home-room-card">
			<div class="home-room-text">
				<h1>Apartment</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure olor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
			<ul>
				<li><a href="{{url('#')}}" class="meghauli-btn-1"><p>Learn More</p></a></li>
				<li><a href="{{url('#')}}" class="meghauli-btn-1"><p>Learn More</p></a></li>
			</ul>
		</div>
		<div class="col-md-6 padding-0">
			<div class="home-room-image">
				<img src="{{url('public/images/3.jpg')}}" alt="" class="img-fluid">
			</div>
		</div>
	</div>
</div>




<div class="container-fluid">
	<div class="row">
		<div class="col-md-6 padding-0">
			<div class="home-room-image">
				<img src="{{url('public/images/2.jpg')}}" alt="" class="img-fluid">
			</div>
		</div>
		<div class="col-md-6 home-room-card">
			<div class="home-room-text">
				<h1>Other rooms</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure olor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
			<ul>
				<li><a href="{{url('#')}}" class="meghauli-btn-1"><p>Learn More</p></a></li>
			</ul>
		</div>
	</div>
</div>
<!-- home room ends -->






<div class="container margin-tb home-activities-title">
	<h1>Our activities</h1>
	<div id="owl-home-activities">
		<div class="item home-activities-item">
			<div class="home-activities-image">
				<img src="{{url('public/images/5.jpg')}}" alt="" class="img-fluid">
			</div>
			<div class="container-fluid padding-0">
				<div class="row">
					<div class="col-md-9">
						<p>Elephant Safari</p>
					</div>
					<div class="col-md-3">
						<p>-></p>
					</div>
				</div>
			</div>
		</div>
		<div class="item home-activities-item">
			<div class="home-activities-image">
				<img src="{{url('public/images/5.jpg')}}" alt="" class="img-fluid">
			</div>
			<div class="container-fluid padding-0">
				<div class="row">
					<div class="col-md-9">
						<p>Elephant Safari</p>
					</div>
					<div class="col-md-3">
						<p>-></p>
					</div>
				</div>
			</div>
		</div>
		<div class="item home-activities-item">
			<div class="home-activities-image">
				<img src="{{url('public/images/5.jpg')}}" alt="" class="img-fluid">
			</div>
			<div class="container-fluid padding-0">
				<div class="row">
					<div class="col-md-9">
						<p>Elephant Safari</p>
					</div>
					<div class="col-md-3">
						<p>-></p>
					</div>
				</div>
			</div>
		</div>
		<div class="item home-activities-item">
			<div class="home-activities-image">
				<img src="{{url('public/images/5.jpg')}}" alt="" class="img-fluid">
			</div>
			<div class="container-fluid padding-0">
				<div class="row">
					<div class="col-md-9">
						<p>Elephant Safari</p>
					</div>
					<div class="col-md-3">
						<p>-></p>
					</div>
				</div>
			</div>
		</div>
		<div class="item home-activities-item">
			<div class="home-activities-image">
				<img src="{{url('public/images/5.jpg')}}" alt="" class="img-fluid">
			</div>
			<div class="container-fluid padding-0">
				<div class="row">
					<div class="col-md-9">
						<p>Elephant Safari</p>
					</div>
					<div class="col-md-3">
						<p>-></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- home our activities ends -->





<div class="container margin-tb home-reviews-title">
	<h1>Our visitors</h1>
	<div id="owl-home-reviews">
		<div class="item">
			<div class="home-reviews-image">
				<img src="{{url('public/images/6.jpg')}}" alt="" class="img-fluid">
			</div>
			<div class="home-reviews-text">
				<h4>John Doe</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			</div>
		</div>
		<div class="item">
			<div class="home-reviews-image">
				<img src="{{url('public/images/6.jpg')}}" alt="" class="img-fluid">
			</div>
			<div class="home-reviews-text">
				<h4>John Doe</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			</div>
		</div>
		<div class="item">
			<div class="home-reviews-image">
				<img src="{{url('public/images/6.jpg')}}" alt="" class="img-fluid">
			</div>
			<div class="home-reviews-text">
				<h4>John Doe</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			</div>
		</div>
		<div class="item">
			<div class="home-reviews-image">
				<img src="{{url('public/images/6.jpg')}}" alt="" class="img-fluid">
			</div>
			<div class="home-reviews-text">
				<h4>John Doe</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			</div>
		</div>
	</div>
</div>
<!-- home visitor ends -->










@endsection