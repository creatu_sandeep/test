<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Page Title -->
    <title>@yield('page-title')</title>


    <!-- FavIcon Link -->
    <link rel="icon" type="image/ico" href="{{url('public/favicon.ico')}}">


    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="description" content="">
    <meta name="keywords" content="">


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{url('public/css/bootstrap.min.css')}}">
    
    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="{{url('public/fonts/The-Heart-of-Everything-Demo.ttf')}}">
    <link rel="stylesheet" type="text/css" href="{{url('public/fonts/HELR45W.ttf')}}">
    
    <!-- Global CSS -->
    <link rel="stylesheet" type="text/css" href="{{url('public/css/style.css')}}">

    <!-- Fancybox CSS -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">

    <!-- Owl Carousel -->
    <link rel="stylesheet" type="text/css" href="{{url('public/css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('public/css/owl.theme.css')}}">

    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{url('public/js/jquery-1.9.1.min.js')}}"></script>

</head>
<body>

    @include('header.header')

    <div style="min-height: 50vh;">
        @yield('content')
    </div>

    @include('footer.footer')

    <a href="#0" class="cd-top js-cd-top">Top</a>
    
</body>

<!-- Popper, Boostrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{url('public/js/bootstrap.js')}}"></script>

<!-- FontAwesome JS -->
<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>

<!-- Fb Messenger -->
<script type="text/javascript" src="{{url('public/js/fb.js')}}"></script>

<!-- Owl Carousel JS -->
<script type="text/javascript" src="{{url('public/js/owl.carousel.min.js')}}"></script>

<!-- Fancybox JS -->
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>

<!-- Global JS -->
<script type="text/javascript" src="{{url('public/js/js.js')}}"></script>

</html>