@extends('home-master')

<!-- page title -->
@section('page-title')	

@endsection


<!-- website content -->
@section('content')
<div class="container-fluid room-bg">
	<div class="container margin-tb room-card">
		<div class="row">
			<div class="col-md-6 padding-0">
				<div class="room-image">
					<img src="{{url('public/images/4.jpg')}}" alt="" class="img-fluid">
				</div>
			</div>
			<div class="col-md-6 room-text-card">
				<div class="room-text">
					<h1>Apartment</h1>
					<ul>
						<li><p>Guests: 5</p></li>
						<li><p>Bed: 3</p></li>
						<li><p>Price: Rs.3000/- per night</p></li>
					</ul>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure olor in reprehenderit in voluptate.</p>
				</div>
				<a href="{{url('room-dynamic')}}" class="meghauli-btn-1"><p>Learn More</p></a>
			</div>
		</div>
	</div>

	<div class="container margin-tb room-card">
		<div class="row">
			<div class="col-md-6 padding-0">
				<div class="room-image">
					<img src="{{url('public/images/4.jpg')}}" alt="" class="img-fluid">
				</div>
			</div>
			<div class="col-md-6 room-text-card">
				<div class="room-text">
					<h1>Apartment</h1>
					<ul>
						<li><p>Guests: 5</p></li>
						<li><p>Bed: 3</p></li>
						<li><p>Price: Rs.3000/- per night</p></li>
					</ul>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure olor in reprehenderit in voluptate.</p>
				</div>
				<a href="{{url('room-dynamic')}}" class="meghauli-btn-1"><p>Learn More</p></a>
			</div>
		</div>
	</div>

	<div class="container margin-tb room-card">
		<div class="row">
			<div class="col-md-6 padding-0">
				<div class="room-image">
					<img src="{{url('public/images/4.jpg')}}" alt="" class="img-fluid">
				</div>
			</div>
			<div class="col-md-6 room-text-card">
				<div class="room-text">
					<h1>Apartment</h1>
					<ul>
						<li><p>Guests: 5</p></li>
						<li><p>Bed: 3</p></li>
						<li><p>Price: Rs.3000/- per night</p></li>
					</ul>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure olor in reprehenderit in voluptate.</p>
				</div>
				<a href="{{url('room-dynamic')}}" class="meghauli-btn-1"><p>Learn More</p></a>
			</div>
		</div>
	</div>
</div>
@endsection