@extends('home-master')

<!-- page title -->
@section('page-title')	

@endsection


<!-- website content -->
@section('content')
<div class="container margin-tb">
	<div class="room-dynamic-title" style="background: linear-gradient(rgba(0,0,0,0.6), rgba(0,0,0,0.5)), url('public/images/3.jpg');background-repeat: no-repeat;background-size: cover; ">
		<h1>Apartment</h1>
		<ul>
			<ul>
				<li><p>Guests: 5</p></li>
				<li><p>Bed: 3</p></li>
				<li><p>Price: Rs.3000/- per night</p></li>
			</ul>
		</ul>
	</div>
</div>

<div class="container margin-tb">
	<div class="row room-dynamic-body">
		<div class="col-md-8">
			<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner home-carousel-card">
					<div class="carousel-item active home-carousel-item">
						<img src="{{url('public/images/3.jpg')}}" class="d-block w-100" alt="...">
					</div>
					<div class="carousel-item home-carousel-item">
						<img src="{{url('public/images/4.jpg')}}" class="d-block w-100" alt="...">
					</div>
					<div class="carousel-item home-carousel-item">
						<img src="{{url('public/images/5.jpg')}}" class="d-block w-100" alt="...">
					</div>
				</div>
				<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>

			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure olor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure olor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>
		<div class="col-md-4">
			<div class="container">
				<div class="row">

					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb30">
						<div class="tour-booking-form">
							<form>
								<div class="row">
									<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
										<h4 class="tour-form-title">Your Travel Plan Detail</h4>
									</div>
									<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
										<div class="form-group">
											<label class="control-label required" for="select">Destination</label>
											<div class="select">
												<select id="select" name="select" class="form-control">
													<option value="">Where you want to go</option>
													<option value="">Singapore</option>
													<option value="">Thailand</option>
													<option value="">Vietnam</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
										<div class="form-group">
											<label class="control-label" for="datepicker">Check in</label>
											<div class="input-group">
												<input id="datepicker" name="datepicker" type="text" placeholder="Date" class="form-control" required>
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span> </div>
											</div>
										</div>
										<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
											<div class="form-group">
												<label class="control-label required" for="select">Number of Persons:</label>
												<div class="select">
													<select id="select" name="select" class="form-control">
														<option value="">Number of Persons:</option>
														<option value="">01</option>
														<option value="">02</option>
														<option value="">03</option>
													</select>
												</div>
											</div>
										</div>
										<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
											<div class="form-group">
												<label class="control-label required" for="select">Budgets</label>
												<div class="select">
													<select id="select" name="select" class="form-control">
														<option value="">Stadard</option>
														<option value="">Stadard</option>
														<option value="">Stadard</option>
													</select>
												</div>
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt30">
											<h4 class="tour-form-title">Your Travel Plan Detail</h4>
										</div>
										<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
											<div class="form-group">
												<label class="control-label" for="name">Name</label>
												<input id="name" type="text" placeholder="First Name" class="form-control" required>
											</div>
										</div>
										<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
											<div class="form-group">
												<label class="control-label" for="email"> Email</label>
												<input id="email" type="text" placeholder="xxxx@xxxx.xxx" class="form-control" required>
											</div>
										</div>
										<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
											<div class="form-group">
												<label class="control-label" for="phone"> Phone</label>
												<input id="phone" type="text" placeholder="(222) 222-2222" class="form-control" required>
											</div>
										</div>
										<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
											<div class="form-group">
												<label class="control-label" for="country">Country</label>
												<input id="country" type="text" placeholder="India" class="form-control" required>
											</div>
										</div>
										<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
											<div class="form-group">
												<label class="control-label" for="city">City</label>
												<input id="city" type="text" placeholder="Ahmedabad" class="form-control" required>
											</div>
										</div>
										<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
											<div class="form-group">
												<label class="control-label" for="textarea">Describe Your Travel Requirements</label>
												<textarea class="form-control" id="textarea" name="textarea" rows="4" placeholder="Write Your Requirements"></textarea>
											</div>
										</div>
										<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
											<button type="submit" name="singlebutton" class="btn btn-primary">send Enquiry</button>
										</div>
									</div>
								</form>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endsection