@extends('home-master')

<!-- page title -->
@section('page-title')	
	
@endsection


<!-- website content -->
@section('content')
	<div class="container margin-tb">
		<div class="activity-dynamic-image">
			<img src="{{url('public/images/3.jpg')}}" alt="" class="img-fluid">
		</div>
	</div>

	<div class="container margin-tb">
		<div class="row">
			<div class="col-md-9">
				<h1>Activity title</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure olor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
			<div class="col-md-3">
				<h3>Highlights</h3>
				<ul>
					<li><p>highlights one</p></li>
					<li><p>highlights two</p></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="container margin-tb">
		<h3>View more activity</h3>
	<div class="row">
		<div class="col-md-4">
			<a href="{{url('activity-dynamic')}}">
				<div class="activity-card">
					<div class="activity-image">
						<img src="{{url('public/images/1.jpg')}}" alt="" class="img-fluid">
					</div>
					<h3>Elephant Safari</h3>
				</div>
			</a>
		</div>
		<div class="col-md-4">
			<a href="{{url('activity-dynamic')}}">
				<div class="activity-card">
					<div class="activity-image">
						<img src="{{url('public/images/1.jpg')}}" alt="" class="img-fluid">
					</div>
					<h3>Elephant Safari</h3>
				</div>
			</a>
		</div>
		<div class="col-md-4">
			<a href="{{url('activity-dynamic')}}">
				<div class="activity-card">
					<div class="activity-image">
						<img src="{{url('public/images/1.jpg')}}" alt="" class="img-fluid">
					</div>
					<h3>Elephant Safari</h3>
				</div>
			</a>
		</div>
	</div>
</div>
@endsection