@extends('home-master')

<!-- page title -->
@section('page-title')	

@endsection


<!-- website content -->
@section('content')
<div class="container margin-tb">
	<div class="row">
		<div class="col-md-4">
			<a href="{{url('activity-dynamic')}}">
				<div class="activity-card">
					<div class="activity-image">
						<img src="{{url('public/images/1.jpg')}}" alt="" class="img-fluid">
					</div>
					<h3>Elephant Safari</h3>
				</div>
			</a>
		</div>
		<div class="col-md-4">
			<a href="{{url('activity-dynamic')}}">
				<div class="activity-card">
					<div class="activity-image">
						<img src="{{url('public/images/1.jpg')}}" alt="" class="img-fluid">
					</div>
					<h3>Elephant Safari</h3>
				</div>
			</a>
		</div>
		<div class="col-md-4">
			<a href="{{url('activity-dynamic')}}">
				<div class="activity-card">
					<div class="activity-image">
						<img src="{{url('public/images/1.jpg')}}" alt="" class="img-fluid">
					</div>
					<h3>Elephant Safari</h3>
				</div>
			</a>
		</div>
		<div class="col-md-4">
			<a href="{{url('activity-dynamic')}}">
				<div class="activity-card">
					<div class="activity-image">
						<img src="{{url('public/images/1.jpg')}}" alt="" class="img-fluid">
					</div>
					<h3>Elephant Safari</h3>
				</div>
			</a>
		</div>
		<div class="col-md-4">
			<a href="{{url('activity-dynamic')}}">
				<div class="activity-card">
					<div class="activity-image">
						<img src="{{url('public/images/1.jpg')}}" alt="" class="img-fluid">
					</div>
					<h3>Elephant Safari</h3>
				</div>
			</a>
		</div>
		<div class="col-md-4">
			<a href="{{url('activity-dynamic')}}">
				<div class="activity-card">
					<div class="activity-image">
						<img src="{{url('public/images/1.jpg')}}" alt="" class="img-fluid">
					</div>
					<h3>Elephant Safari</h3>
				</div>
			</a>
		</div>
	</div>
</div>
@endsection