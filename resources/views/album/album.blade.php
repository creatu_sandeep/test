@extends('home-master')

<!-- page title -->
@section('page-title')	

@endsection


<!-- website content -->
@section('content')
<div class="container margin-tb album-title">
	<h1>Our Gallery</h1>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure olor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
</div>

<div class="container margin-tb">
	<div class="row">
		<div class="col-md-6">
			<div class="album-image">
				<a href="{{url('gallery')}}"><img class="img-fluid" src="{{url('public/images/1.jpg')}}" alt=""></a>
			</div>
		</div>

		<div class="col-md-6">
			<div class="album-image">
				<a href="{{url('gallery')}}"><img class="img-fluid" src="{{url('public/images/2.jpg')}}" alt=""></a>
			</div>
		</div>
	</div>
</div>
@endsection