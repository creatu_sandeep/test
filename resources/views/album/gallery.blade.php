@extends('home-master')

<!-- page title -->
@section('page-title')	

@endsection


<!-- website content -->
@section('content')
<div class="container gallery-title margin-tb">
	<h3>Album Name</h3>
</div>

<div class="container margin-tb">
	<div class="row">
		<div class="col-md-4">
			<div class="gallery-image">
				<a class="fancybox" href="{{url('public/images/2.jpg')}}" rel="lightbox">
					<img class="img-fluid" src="{{url('public/images/2.jpg')}}" alt=""/>
				</a>

			</div>
		</div>

		<div class="col-md-4">

			<div class="gallery-image">
				<a class="fancybox" href="{{url('public/images/3.jpg')}}" rel="lightbox">
					<img class="img-fluid" src="{{url('public/images/3.jpg')}}" alt=""/>
				</a>
			</div>
		</div>

		<div class="col-md-4">
			<div class="gallery-image">
				<a class="fancybox" href="{{url('public/images/4.jpg')}}" rel="lightbox">
					<img class="img-fluid" src="{{url('public/images/4.jpg')}}" alt=""/>
				</a>

			</div>
		</div>

		<div class="col-md-4">

			<div class="gallery-image">
				<a class="fancybox" href="{{url('public/images/1.jpg')}}" rel="lightbox">
					<img class="img-fluid" src="{{url('public/images/1.jpg')}}" alt=""/>
				</a>
			</div>
		</div>

		<div class="col-md-4">
			<div class="gallery-image">
				<a class="fancybox" href="{{url('public/images/2.jpg')}}" rel="lightbox">
					<img class="img-fluid" src="{{url('public/images/2.jpg')}}" alt=""/>
				</a>

			</div>
		</div>

		<div class="col-md-4">

			<div class="gallery-image">
				<a class="fancybox" href="{{url('public/images/3.jpg')}}" rel="lightbox">
					<img class="img-fluid" src="{{url('public/images/3.jpg')}}" alt=""/>
				</a>
			</div>
		</div>
	</div>
</div>	


<script type="text/javascript">
	$(document).ready(function(){
    //FANCYBOX
    //https://github.com/fancyapps/fancyBox
    $(".fancybox").fancybox({
    	openEffect: "none",
    	closeEffect: "none"
    });
});
</script>
@endsection