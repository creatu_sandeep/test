@extends('home-master')

<!-- page title -->
@section('page-title')	
about | MWL
@endsection


<!-- website content -->
@section('content')
<div class="container margin-tb about-title">
	<h1>Welcome to meghauli wildlife resort</h1>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure olor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
</div>

<div class="container margin-tb about-service-title">
	<h1>Services we provides</h1>
	<div class="row">
		<div class="col-md-4 about-service-card">
			<div class="about-service-image">
				<img src="{{url('public/images/1.jpg')}}" alt="" class="img-fluid">
			</div>
			<h3>Free wifi</h3>
		</div>
		<div class="col-md-4 about-service-card">
			<div class="about-service-image">
				<img src="{{url('public/images/1.jpg')}}" alt="" class="img-fluid">
			</div>
			<h3>Free wifi</h3>
		</div>
		<div class="col-md-4 about-service-card">
			<div class="about-service-image">
				<img src="{{url('public/images/1.jpg')}}" alt="" class="img-fluid">
			</div>
			<h3>Free wifi</h3>
		</div>
		<div class="col-md-4 about-service-card">
			<div class="about-service-image">
				<img src="{{url('public/images/1.jpg')}}" alt="" class="img-fluid">
			</div>
			<h3>Free wifi</h3>
		</div>
		<div class="col-md-4 about-service-card">
			<div class="about-service-image">
				<img src="{{url('public/images/1.jpg')}}" alt="" class="img-fluid">
			</div>
			<h3>Free wifi</h3>
		</div>
		<div class="col-md-4 about-service-card">
			<div class="about-service-image">
				<img src="{{url('public/images/1.jpg')}}" alt="" class="img-fluid">
			</div>
			<h3>Free wifi</h3>
		</div>
	</div>
</div>



<div class="container-fluid">
	<div class="row">
		<div class="col-md-6 padding-0">
			<div class="home-room-image">
				<img src="{{url('public/images/2.jpg')}}" alt="" class="img-fluid">
			</div>
		</div>
		<div class="col-md-6 home-room-card">
			<div class="home-room-text">
				<h1>Learn about our rooms</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure olor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
			<ul>
				<li><a href="{{url('#')}}" class="meghauli-btn-1"><p>Learn More</p></a></li>
			</ul>
		</div>
	</div>
</div>


<div class="container-fluid">
	<div class="row">
		<div class="col-md-6 home-room-card">
			<div class="home-room-text">
				<h1>Learn about our activities</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure olor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
			<ul>
				<li><a href="{{url('#')}}" class="meghauli-btn-1"><p>Learn More</p></a></li>
			</ul>
		</div>
		<div class="col-md-6 padding-0">
			<div class="home-room-image">
				<img src="{{url('public/images/2.jpg')}}" alt="" class="img-fluid">
			</div>
		</div>
	</div>
</div>
@endsection